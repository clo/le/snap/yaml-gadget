# SPDX-License-Identifier: GPL-2.0
# Copyright (C) 2021 Canonical Ltd
name: qti-sa8155p-adp
summary: SA8155 Automotive Development Platform gadget snap
description: |
 Support files for booting Automotive Development Platform

adopt-info: sign-assets
type: gadget
base: core22

architectures:
  - build-on: [amd64, arm64]
    build-for: arm64

confinement: strict
grade: stable

apps:
  init-post-boot:
    daemon: oneshot
    command: usr/bin/init_post_boot.sh start
    plugs:
      - cpu-control
      - power-control
      - post-boot-init

parts:
  prebuilt:
    plugin: dump
    source: prebuilt
    source-type: local
    override-pull: |
      # allow custom source definition
      if [ -n "${SA8155P_GADGET_SNAP_BOOT_ASSETS:-}" ]; then
        if [ -d ${SA8155P_GADGET_SNAP_BOOT_ASSETS} ]; then
          cp -r ${SA8155P_GADGET_SNAP_BOOT_ASSETS}/* ${CRAFT_PART_SRC}
        else
          git clone --depth 1 ${SA8155P_GADGET_SNAP_BOOT_ASSETS} ${CRAFT_PART_SRC}
        fi
      else
        echo "!!! Missing env SA8155P_GADGET_SNAP_BOOT_ASSETS pointing to boot assets"
        exit 1
      fi
    organize:
      '*': blobs/

  init-post-boot:
    plugin: nil
    override-build: |
      install -m 775 -D ${CRAFT_PROJECT_DIR}/init_post_boot.sh ${CRAFT_PART_INSTALL}/usr/bin/init_post_boot.sh

  abl:
    plugin: nil
    source: https://git.launchpad.net/~ubuntu-cervinia/+git/edk2
    source-type: git
    source-branch: uefi.lnx.1.9.1.r3-rel-uc20
    source-depth: 1
    override-pull: |
      # allow custom source definition
      if [ -n "${SA8155P_GADGET_SNAP_EDK2:-}" ]; then
        if [ -d ${SA8155P_GADGET_SNAP_EDK2} ]; then
          cp -r ${SA8155P_GADGET_SNAP_EDK2}/* ${CRAFT_PART_SRC}
        else
          git clone --depth 1 ${SA8155P_GADGET_SNAP_EDK2} \
              ${CRAFT_PART_SRC}
        fi
      else
        craftctl default
      fi
    override-build: |
      [ -e out ] && rm -rf out
      # make sure default python is 2.x
      mkdir -p bin
      ln -f -s $(which python2) bin/python
      # make sure we default to clang-11
      sudo update-alternatives --install /usr/bin/clang clang /usr/bin/clang-11 100
      sudo update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-11 100
      export PATH="${CRAFT_PART_BUILD}/bin:${PATH}"
      export CC=gcc
      export CXX=g++
      export LD=ld
      export AR=ar
      export LINUX_BOOT_CPU_SELECTION_ENABLED=1
      export TARGET_LINUX_BOOT_CPU_ID=7
      make -j 1 \
          'CLANG_BIN=/usr/bin/' \
          'CLANG_PREFIX=/usr/bin/aarch64-linux-gnu-' \
          'TARGET_ARCHITECTURE=aarch64' \
          'BUILDDIR=${PWD}' \
          'BOOTLOADER_OUT=${PWD}/out' \
          'ENABLE_LE_VARIANT=false' \
          'VERIFIED_BOOT_LE=1' \
          'VERITY_LE=0' \
          'UBUNTU_CORE_BOOT=1' \
          'EDK_TOOLS_PATH=${PWD}/BaseTools' \
          'EARLY_ETH_ENABLED=1' \
          'UBSAN_UEFI_GCC_FLAG_ALIGNMENT=-Wno-misleading-indentation' \
          'TARGET_BOARD_TYPE_AUTO=1'  \
          'BOOTLOADER_PLATFORM=msmcobalt' \
          'SIGNED_KERNEL=1' \
          'USER_BUILD_VARIANT=0' \
          'DEVICE_STATUS=DEFAULT_UNLOCK=true' \
          -f makefile all
      cp out/Build/DEBUG_CLANG35/FV/abl.elf ${CRAFT_PART_INSTALL}/abl.elf.nonsecure
      # clean clang alternatives
      sudo update-alternatives --remove-all clang
      sudo update-alternatives --remove-all clang++
    organize:
      '*': blobs/

  gpt-partitions:
    plugin: nil
    source: qdl
    after:
      - partioning-tool
    override-build: |
      echo "Updating partition.xml sizes from gadget.yaml"
      convert_size() {
        case $1 in
          (0)  echo "0" ;;
          (*G | *g) echo $(expr $(echo ${1} | sed -e 's/G//g' -e 's/g//g') \* 1024 \* 1024) ;;
          (*M | *m) echo $(expr $(echo ${1} | sed -e 's/M//g' -e 's/m//g') \* 1024) ;;
          (*K | *k) echo $(echo ${1} | sed -e 's/K//g' -e 's/k//g') ;;
          (*) echo $(expr ${1} / 1024) ;;
        esac
      }
      for p_name in $(yq eval '.volumes.[].structure.[].name' ${SNAPCRAFT_PROJECT_DIR}/gadget.yaml)
      do
        export p=${p_name}
        p_size=$(yq eval '.volumes.[].structure.[] | select(.name == env(p)) | .size' ${SNAPCRAFT_PROJECT_DIR}/gadget.yaml)
        p_size=$(convert_size ${p_size})
        echo -e "OK: partition: ${p_name} - ${p_size}"
        sed -i \
          -e 's/\"'"${p_name}"'-size\"/\"'"${p_size}"'\"/g' \
          partition.xml
      done
      python3 ${CRAFT_STAGE}/ptool.py -x partition.xml -t .
      [ -e rawprogram0_BLANK_GPT.xml ] && rm rawprogram*_BLANK_GPT.xml
      [ -e rawprogram0_WIPE_PARTITIONS.xml ] && rm rawprogram*_WIPE_PARTITIONS.xml
      cp rawprogram*.xml ${CRAFT_PART_INSTALL}
      cp patch*.xml ${CRAFT_PART_INSTALL}
      cp gpt_* ${CRAFT_PART_INSTALL}
    build-snaps:
      - yq
    organize:
      '*': blobs/
    stage:
      - blobs/gpt_b*.bin
      - blobs/gpt_main*.bin
      - blobs/patch*.xml
      - blobs/rawprogram*.xml

  snap-boot-sel-env:
    plugin: nil
    source: snap-boot-sel
    source-type: local
    override-pull: |
      craftctl default
      wget https://raw.githubusercontent.com/snapcore/snapd/master/include/lk/snappy_boot_common.h
      wget https://raw.githubusercontent.com/snapcore/snapd/master/include/lk/snappy_boot_v2.h
    override-build: |
      gcc lk-boot-env.c -I/usr/include/ -o lk-boot-env
      # configure initial boot environment
      ./lk-boot-env --runtime --write ${CRAFT_PART_INSTALL}/snapbootsel.bin \
                    --bootimg-file boot.img \
                    $( ./parse-yaml.sh ${CRAFT_PROJECT_DIR}/gadget.yaml runtime )
      ./lk-boot-env --recovery --write ${CRAFT_PART_INSTALL}/snaprecoverysel.bin \
                    --bootimg-file boot.img \
                    $( ./parse-yaml.sh ${CRAFT_PROJECT_DIR}/gadget.yaml recovery )
      ln -sf snaprecoverysel.bin ${CRAFT_PART_INSTALL}/lk.conf
    organize:
      snapbootsel.bin: blobs/snapbootsel.bin

  partioning-tool:
    plugin: dump
    source: https://github.com/kubiko/partitioning_tool.git
    source-depth: 1
    source-type: git
    organize:
      LICENSE: LICENSE_for_ptool
    prime:
      - -'*'

  sign-assets:
    plugin: nil
    after:
      - abl
      - prebuilt
    override-pull: |
      # allow custom source definition
      if [ -n "${SA8155P_GADGET_SNAP_SECTOOLS:-}" ]; then
        if [ -d ${SA8155P_GADGET_SNAP_SECTOOLS} ]; then
          ln -sf ${SA8155P_GADGET_SNAP_SECTOOLS} \
              ${CRAFT_PART_SRC}/sectools
        else
          git clone --depth 1 ${SA8155P_GADGET_SNAP_SECTOOLS} \
              ${CRAFT_PART_SRC}/sectools
        fi
      else
        echo "!! Missing env SA8155P_GADGET_SNAP_SECTOOLS pointing to sectools"
        echo "!! Produced snap won't be bootable"
      fi
    override-build: |
      SNAP_VERSION="22-2"
      if [ -e ${CRAFT_PART_BUILD}/sectools/sectools.py ]; then
        # we have sectools, sign boot assets
        ${CRAFT_PROJECT_DIR}/sign-boot-assets \
          sign \
          --sectools ${CRAFT_PART_BUILD}/sectools/sectools.py \
          --config ${CRAFT_PART_BUILD}/sectools/config/sm8150/sm8150_secimage.xml \
          --in ${CRAFT_STAGE}/blobs \
          --out ${CRAFT_PART_INSTALL} \
          --workdir ${CRAFT_PART_BUILD}
        craftctl set version=${SNAP_VERSION}
        python2 ${CRAFT_PART_BUILD}/sectools/sectools.py \
          fuseblower -p sm8150 -gad --sign -o secdatdir -v
        SA8155P_GADGET_SNAP_ENABLE_SEC_ELF="${SA8155P_GADGET_SNAP_ENABLE_SEC_ELF:-0}"
        if [ "$SA8155P_GADGET_SNAP_ENABLE_SEC_ELF" -eq 1 ]; then
          cp secdatdir/fuseblower_output/v2/sec.elf ${CRAFT_PART_INSTALL}/sec.elf
        else
          cp secdatdir/fuseblower_output/v2/sec.elf ${CRAFT_PART_INSTALL}/sec.elf.disabled
        fi
      else
        # create sym links to unsigned blobs to satisfy build
        for a in $(ls ${CRAFT_STAGE}/blobs)
        do
          filename="$(basename ${a})"
          extension="${filename##*.}"
          if [ "${extension}" = "nonsecure" ] || [ "${extension}" = "qsigned" ]; then
            ln -sf ${filename} \
                  ${CRAFT_PART_INSTALL}/${filename%.*}
          fi
        done
        craftctl set version=${SNAP_VERSION}-unsigned
      fi
    organize:
      '*': blobs/
    stage:
      - blobs/

  hooks:
    plugin: dump
    source: hooks
    source-type: local
    override-pull: |
      # allow custom source definition
      if [ -n "${SA8155P_GADGET_SNAP_HOOKS:-}" ]; then
        if [ -d ${SA8155P_GADGET_SNAP_HOOKS} ]; then
          cp -r ${SA8155P_GADGET_SNAP_HOOKS}/* ${CRAFT_PART_SRC}
        else
          git clone --depth 1 ${SA8155P_GADGET_SNAP_HOOKS} ${CRAFT_PART_SRC}
        fi
      else
        echo "Missing env SA8155P_GADGET_SNAP_HOOKS no hooks will be included"
      fi
    organize:
      '*': meta/hooks/

plugs:
  post-boot-init:
    interface: system-files
    read:
      - /sys/devices/soc0/machine
    write:
      - /sys/devices/platform/soc/780130.qfprom/qfprom0/nvmem
      - /sys/devices/platform/soc/soc:qcom,cpu-cpu-llcc-bw
      - /sys/devices/platform/soc/soc:qcom,cpu-llcc-ddr-bw
      - /sys/devices/platform/soc/soc:qcom,npu-npu-ddr-bw
      - /sys/devices/platform/soc/soc:qcom,cpu0-cpu-l3-lat
      - /sys/devices/platform/soc/soc:qcom,cpu0-cpu-llcc-lat
      - /sys/devices/platform/soc/soc:qcom,cpu0-llcc-ddr-lat
      - /sys/devices/platform/soc/soc:qcom,cpu4-cpu-l3-lat
      - /sys/devices/platform/soc/soc:qcom,cpu4-cpu-llcc-lat
      - /sys/devices/platform/soc/soc:qcom,cpu4-llcc-ddr-lat
      - /sys/devices/platform/soc/soc:qcom,cpu7-cpu-l3-lat
      - /sys/devices/platform/soc/soc:qcom,cpu4-cpu-ddr-latfloor
      - /sys/devices/platform/soc/soc:qcom,cpu4-cpu-l3-lat
      - /sys/devices/platform/soc/soc:qcom,cpu7-cpu-l3-lat
      - /sys/devices/virtual/npu
      - /sys/module/lowmemorykiller/parameters/oom_reaper
      - /proc/sys/vm/watermark_scale_factor
      - /proc/sys/vm/reap_mem_on_sigkill

slots:
  volume-ssd:
    interface: raw-volume
    path: /dev/sda1
  gnss-serial:
    interface: serial-port
    path: /dev/ttyHS1
  gnss-custom-devices:
    interface: custom-device
    devices:
      - /dev/at_usb0
      - /dev/gnss_sirf
  ice-custom-devices:
    interface: custom-device
    devices:
      - /dev/iceufs

build-packages:
    - wget
    - python2
    - python3
    - clang-11
    - gcc-aarch64-linux-gnu
    - gwenhywfar-tools
    - build-essential
