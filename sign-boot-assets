#!/bin/bash
# SPDX-License-Identifier: GPL-2.0
# Copyright (C) 2021 Canonical Ltd

# mapping array of default files names to sign_id
declare -A SIGN_IDS
SIGN_IDS["abl.elf"]="abl"
SIGN_IDS["aop.mbn"]="aop"
SIGN_IDS["cmnlib64.mbn"]="cmnlib64"
SIGN_IDS["cmnlib.mbn"]="cmnlib"
SIGN_IDS["devcfg_auto.mbn"]="devcfg"
SIGN_IDS["hyp.mbn"]="hyp"
SIGN_IDS["km4.mbn"]="keymaster"
SIGN_IDS["multi_image.mbn"]="multi_image"
SIGN_IDS["prog_firehose_ddr.elf"]="prog_firehose_ddr"
SIGN_IDS["qupv3fw.elf"]="qupv3"
SIGN_IDS["storsec.mbn"]="storsec"
SIGN_IDS["uefi_sec.mbn"]="uefisecapp"
SIGN_IDS["xbl_config.elf"]="xbl_config"
SIGN_IDS["xbl.elf"]="xbl"
SIGN_IDS["tz.mbn"]="tz"

print_help() {
  echo -e "There are two modes to call this helper script"
  echo -e "sign: at snap build time, to sign boot assets"
  echo -e "sign-repack: repack existing gadget snap and (re)sign boot assets with new key"
  echo -e "\nArguments:"
  echo -e "mandatory:"
  echo -e "\tmode of operation: mandatory: sign or sign-repack"
  echo -e "\t--sectools: mandaroty path to the sectool.py"
  echo -e "\t--config: mandatory path to the config file to be used for signing"
  echo -e "\nMandatory based on operation mode"
  echo -e "\tsign:"
  echo -e "\t\t--in: input directory with unsigned boot assets"
  echo -e "\t\t--out: output directory to place signed boot assets"
  echo -e "\tsign-repack:"
  echo -e "\t\t--gadget-snap: path to the gadget snap to repack"
  echo -e "\nOptional parameters:"
  echo -e "\t--workdir: work directory. If it does not exist it will be created."
  echo -e "\t\tIf not supplied tmp dir is creasted"
  echo -e "--help: Print this help"
}

unpack_snap() {
  local snap="${1}"
  local workdir="${2}"
  if [ -e ${workdir} ]; then
    rm -rf ${workdir}
  fi
  unsquashfs -f -d ${workdir} ${snap}
}

sign_assets() {
  local sectools="${1}"
  local sectools_config="${2}"
  local input_dir="${3}"
  local output_dir="${4}"
  local work_dir="${5}"
  local chipset=$(grep "<chipset>" ${sectools_config} | awk -F '[><]' '{print $3}')
  for a in $(ls ${input_dir})
  do
    local filename="$(basename ${a})"
    local extension="${filename##*.}"
    if [ "${extension}" = "nonsecure" ] || [ "${extension}" = "qsigned" ]; then
      local sign_id=${SIGN_IDS[${filename%.*}]}
      python2 ${sectools} \
          secimage \
            --verbose \
            --config_path=${sectools_config} \
            --sign_id=${sign_id} \
            --image_file=${input_dir}/${filename} \
            --sign \
            --validate \
            --output_dir=${work_dir}
      if [ -e ${work_dir}/${chipset}/${sign_id}/${filename} ]; then
        mv ${work_dir}/${chipset}/${sign_id}/${filename} \
           ${output_dir}/${filename%.*}.signed
      else
        # some signed images do not hour input file name,
        # try without .nonsecure|qsigned
        mv ${work_dir}/${chipset}/${sign_id}/${filename%.*} \
           ${output_dir}/${filename%.*}.signed
      fi
      ln -sf ${filename%.*}.signed \
             ${output_dir}/${filename%.*}
    fi
  done
  chmod +x ${output_dir}/*
}


# parse mode of op arguments
if [ "${1}" = "sign" ]; then
  OP="sign"
elif [ "${1}" = "sign-repack" ]; then
  OP="sign-repack"
elif [ "${1}" = "--help" ]; then
  print_help
  exit 0
else
  echo "Unknown operation: ${1}"
  print_help
  exit 0
fi
shift

# parse rest of the arguments
while [ "$1" != "" ]
do
  case "${1}" in
    --sectools)
      if [ -e "${2}" ]; then
        SECTOOLS="${2}"
      else
        echo -e "Passed sectools are not valid: ${2}"
        exit 0
      fi
      shift
      ;;
    --config)
      if [ -e "${2}" ]; then
        SECTOOLS_CONFIG="${2}"
      else
        echo -e "Passed sectools config does not exist: ${2}"
        exit 0
      fi
      shift
      ;;
    --in)
      if [ -d "${2}" ]; then
        BOOT_ASSETS="${2}"
      else
        echo -e "Passed input directory does not exist: ${2}"
        exit 0
      fi
      shift
      ;;
    --out)
      OUT_DIR="${2}"
      shift
      ;;
    --gadget-snap)
      if [ -e "${2}" ]; then
        GADGET_SNAP="${2}"
      else
        echo -e "Passed gadget snap does not exist: ${2}"
        exit 0
      fi
      shift
      ;;
    --workdir)
      WORK_DIR="${2}"
      shift
      ;;
    --help|-h)
      print_help
      exit 0
      ;;
    *)
      echo "Unknown argument: ${1}"
      exit 0
      ;;
  esac
  shift
done


# check minimal params passed
if [ -z "${SECTOOLS}" ] ||  [ -z "${SECTOOLS_CONFIG}" ]; then
  echo -e "Missing mandatory parameters [sectools/sectools config]"
  exit 0
fi

# check if we need to created work dir
if [ -z "${WORK_DIR}" ]; then
  WORK_DIR=$(mktemp -d)
  CLEAN_WORK_DIR="true"
fi
# handle the operation
if [ "${OP}" = "sign" ]; then
  if [ -z "${BOOT_ASSETS}" ] || [ -z "${OUT_DIR}" ]; then
    echo -e "Missing mandatory parameters [boot assets/out dir]"
    exit 0
  fi
  sign_assets ${SECTOOLS} ${SECTOOLS_CONFIG} ${BOOT_ASSETS} ${OUT_DIR} ${WORK_DIR}
elif [ "${OP}" = "sign-repack" ]; then
  if [ -z "${GADGET_SNAP}" ]; then
    echo -e "Missing mandatory parameters [gadget snap]"
    exit 0
  fi
  unpack_snap ${GADGET_SNAP} ${WORK_DIR}
  SIGNING_DIR=$(mktemp -d)
  sign_assets ${SECTOOLS} ${SECTOOLS_CONFIG} ${WORK_DIR}/blobs ${WORK_DIR}/blobs ${SIGNING_DIR}
  rm -rf ${SIGNING_DIR}
  # remove "-unsigned" from snap version if there
  sed -i 's/version: \(.*\)-unsigned\(.*\)/version: \1\2/g' ${WORK_DIR}/meta/snap.yaml
  snap pack ${WORK_DIR}
fi

# if [ "${CLEAN_WORK_DIR}" = "true" ]; then
#   rm -rf ${WORK_DIR}
# fi
